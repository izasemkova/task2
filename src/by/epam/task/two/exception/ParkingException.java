package by.epam.task.two.exception;

public class ParkingException extends Exception {
	private static final long serialVersionUID = 1L;

	public ParkingException(String message){
		super(message);
	}
}
