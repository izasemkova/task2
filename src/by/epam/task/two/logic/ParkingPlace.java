package by.epam.task.two.logic;

public class ParkingPlace {
	private int id;

	public ParkingPlace(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

}
