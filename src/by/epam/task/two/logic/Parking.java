package by.epam.task.two.logic;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

public class Parking {
	private final static Logger logger = Logger.getRootLogger();

	private BlockingQueue<ParkingPlace> parkingPlaceList;

	// The process of creating parking with necessary amount parking places
	public Parking(int parkingSize) {
		// Creating queue of free parking places.
		// The size of queue is a size of parking.
		setParkingPlaceList(new ArrayBlockingQueue<ParkingPlace>(parkingSize));
		for (int i = 0; i < parkingSize; i++) {
			// Filling queue by parking places.
			getParkingPlaceList().add(new ParkingPlace(i));
		}
		logger.debug("Парковка на " + parkingSize + " машиномест создана.");
	}

	public BlockingQueue<ParkingPlace> getParkingPlaceList() {
		return parkingPlaceList;
	}

	public void setParkingPlaceList(BlockingQueue<ParkingPlace> parkingPlaceList) {
		this.parkingPlaceList = parkingPlaceList;
	}
}
