package by.epam.task.two.logic;

import org.apache.log4j.Logger;

import by.epam.task.two.exception.ParkingException;

public class Car implements Runnable {

	private final static Logger logger = Logger.getRootLogger();
	private volatile boolean stopThread = false;
	private static ParkingAttendant parkingAttendant = new ParkingAttendant();

	private String name;
	private Parking parking;
	private int timeout;

	// Creating car with name, parking and time this car will be waiting in
	// queue
	public Car(String name, Parking parking, int timeout) {
		super();
		this.name = name;
		this.parking = parking;
		this.timeout = timeout;
	}

	public void stopThread() {
		stopThread = true;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Parking getParking() {
		return parking;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	@Override
	public void run() {
		try {
			while (!stopThread) {
				onTheWay();
				atTheParking();
			}
		} catch (InterruptedException e) {
			logger.error("Автомобиль попал в аварию.", e);
		} catch (ParkingException e) {
			logger.error("Автомобиль украли с автостоянки.", e);
		}
	}

	private void onTheWay() throws InterruptedException {
		Thread.sleep(1000);
	}

	private void atTheParking() throws ParkingException, InterruptedException {
		boolean isLockedParkingPlace = false;
		ParkingPlace parkingPlace = null;
		try {
			// Locking parking places for current car
			isLockedParkingPlace = parkingAttendant.lockParkingPlace(this);
			if (isLockedParkingPlace) {
				// Taking parking places from Map of used parking places for
				// current car
				parkingPlace = parkingAttendant.getParkingPlace(this);
				logger.debug("Автомобиль " + name + " занял машиноместо № " + parkingPlace.getId());
				// Exchange parking places with neighbor for current car
				parkingAttendant.changeParkingPlace(this, parkingPlace);
			} else {
				logger.debug("Автомобиль " + name + " устал ждать свободных мест и уехал.");
			}
		} catch (InterruptedException e) {
			logger.debug("Автомобилю " + name + " не выделили машиноместо.");

		} finally {
			if (isLockedParkingPlace) {
				try {
					// Unlocking parking places for current car
					parkingPlace = parkingAttendant.unlockParkingPlace(this);
					logger.debug("Автомобиль " + name + " освободил машиноместо № " + parkingPlace.getId());
				} catch (InterruptedException e) {
					logger.debug("Автомобиль " + name + " не смог уехать со стоянки.");
				}
			}
		}
	}
}
