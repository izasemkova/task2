package by.epam.task.two.logic;

import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import by.epam.task.two.exception.ParkingException;

public class ParkingAttendant {

	private final static Logger logger = Logger.getRootLogger();
	// Map of used parking places
	private static Map<Car, ParkingPlace> usedParkingPlaces = new ConcurrentHashMap<Car, ParkingPlace>();
	private static Lock lock = new ReentrantLock();

	Condition isFree;

	// The process of locking parking places
	public boolean lockParkingPlace(Car car) throws InterruptedException {
		ParkingPlace parkingPlace = null;
		// Taking parking
		Parking parking = car.getParking();
		// Taking queue of free places
		BlockingQueue<ParkingPlace> parkingPlaceList = parking.getParkingPlaceList();
		// Returning a Condition instance to be used with this Lock instance.
		isFree = lock.newCondition();
		try {
			// Acquires the lock if it is not held by another thread
			lock.lock();
			// Taking free parking place from queue
			parkingPlace = parkingPlaceList.poll(car.getTimeout(), TimeUnit.MICROSECONDS);
			if (parkingPlace != null) {
				// if we have such place, we will write car and place to the
				// used parking place Map.
				usedParkingPlaces.put(car, parkingPlace);
			} else {
				return false;
			}
			// Wakes up all waiting threads.
			isFree.signalAll();
		} finally {
			// Releasing the lock.
			lock.unlock();
		}
		return true;
	}

	// The process of  taking parking places from Map
	public ParkingPlace getParkingPlace(Car car) throws ParkingException {
		// Taking parking places from Map of used parking places for given car
		ParkingPlace parkingPlace = usedParkingPlaces.get(car);
		if (parkingPlace == null) {
			throw new ParkingException("Автомобиль " + " попытался несанкционированно воспользоваться стоянокой.");
		}
		return parkingPlace;
	}

	// The process of exchange parking places with neighbor
	public void changeParkingPlace(Car car, ParkingPlace parkingPlace) throws InterruptedException, ParkingException {
		// Returning a Condition instance to be used with this Lock instance.
		isFree = lock.newCondition();
		try {
			// Acquires the lock if it is not held by another thread
			lock.lock();
			// Random method for choosing a neighbor with smaller or larger
			// parking place id
			Random random = new Random();
			int value = random.nextInt(5000);
			ParkingPlace currentPlace = parkingPlace;
			Car neighbor = null;
			if ((value < 2500) && (currentPlace != null)) {
				// Searching neighbor car using delta of id
				neighbor = findNeighborCar(car, currentPlace, -1);
				if (neighbor != null) {
					// Exchanging parking places for two cars
					changeCars(car, neighbor);
				}
			} else if ((value > 2500) && (currentPlace != null)) {
				// Searching neighbor car using delta of id
				neighbor = findNeighborCar(car, currentPlace, 1);
				if (neighbor != null) {
					// Exchanging parking places for two cars
					changeCars(car, neighbor);
				}
			}
			// Wakes up all waiting threads.
			isFree.signalAll();
		} finally {
			// Releasing the lock.
			lock.unlock();
		}
	}

	// The process of searching neighbor car using delta of parking places id
	// using iterator
	public Car findNeighborCar(Car car, ParkingPlace parkingPlace, int id) throws ParkingException {
		Set<Map.Entry<Car, ParkingPlace>> entrySet = usedParkingPlaces.entrySet();
		Car neighbor = null;
		ParkingPlace currentPlace = parkingPlace;
		if (currentPlace != null) {
			Iterator<Map.Entry<Car, ParkingPlace>> iterator = entrySet.iterator();
			while (iterator.hasNext()) {
				Map.Entry<Car, ParkingPlace> item = iterator.next();
				if (item != null) {
					if ((currentPlace.getId() + id) == item.getValue().getId()) {
						neighbor = item.getKey();
					}
				}
			}
		}
		return neighbor;
	}

	// The process of exchanging parking places for two cars
	public void changeCars(Car car1, Car car2) throws InterruptedException, ParkingException {
		// Returning a Condition instance to be used with this Lock instance.
		Condition isFree = lock.newCondition();
		try {
			// Acquires the lock if it is not held by another thread
			lock.lock();
			// Taking parking places from Map of used parking places for first car
			ParkingPlace currentPlace = usedParkingPlaces.get(car1);
			// Taking parking places from Map of used parking places for second car
			ParkingPlace finalPlace = usedParkingPlaces.get(car2);
			if ((finalPlace != null) && (finalPlace != currentPlace)) {
				//Removing first car from Map
				usedParkingPlaces.remove(car1);
				//Removing second car from Map
				usedParkingPlaces.remove(car2);
				//Placing the first car into Map with new parking place
				usedParkingPlaces.put(car1, finalPlace);
				//Placing the second car into Map with new parking place
				usedParkingPlaces.put(car2, currentPlace);
				logger.debug("Автомобиль " + car1.getName() + " обменялся местами  с автомобилем " + car2.getName()
						+ ".\n Автомобиль " + car1.getName() + " занял место " + finalPlace.getId() + ".\n Автомобиль "
						+ car2.getName() + " занял место " + currentPlace.getId());

			} else {
				logger.debug("Автомобиль " + car1.getName() + " не смог обменятся местами с соседним автомобилем.");
			}
			// Wakes up all waiting threads.
			isFree.signalAll();
		} finally {
			// Releasing the lock.
			lock.unlock();
		}

	}

	// The process of unlocking parking places
	public ParkingPlace unlockParkingPlace(Car car) throws InterruptedException {
		// Taking parking places from Map of used parking places for given car
		ParkingPlace parkingPlace = usedParkingPlaces.get(car);
		// Taking parking
		Parking parking = car.getParking();
		// Taking queue of free places
		BlockingQueue<ParkingPlace> parkingPlaceList = parking.getParkingPlaceList();
		// Returning a Condition instance to be used with this Lock instance.
		isFree = lock.newCondition();
		try {
			// Acquires the lock if it is not held by another thread
			lock.lock();
			// Placing free parking place into queue
			parkingPlaceList.put(parkingPlace);
			parking.setParkingPlaceList(parkingPlaceList);
			//Removing car from Map
			usedParkingPlaces.remove(car);
			// Wakes up all waiting threads.
			isFree.signalAll();
		} finally {
			// Releasing the lock.
			lock.unlock();
		}
		return parkingPlace;
	}

}
