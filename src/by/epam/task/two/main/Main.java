package by.epam.task.two.main;

import by.epam.task.two.logic.Car;
import by.epam.task.two.logic.Parking;

public class Main {
	public static void main(String[] args) throws InterruptedException {
		//Creating parking for 3 parking places
		Parking parking = new Parking(3);

		//Creating cars with name, parking and time this car will be waiting in queue
		Car car1 = new Car("Car1", parking, 2);
		Car car2 = new Car("Car2", parking, 4);
		Car car3 = new Car("Car3", parking, 6);
		Car car4 = new Car("Car4", parking, 8);
		Car car5 = new Car("Car5", parking, 3);
		Car car6 = new Car("Car6", parking, 5);
		Car car7 = new Car("Car7", parking, 7);

		//Starting threads for every car
		new Thread(car1).start();
		new Thread(car2).start();
		new Thread(car3).start();
		new Thread(car4).start();
		new Thread(car5).start();
		new Thread(car6).start();
		new Thread(car7).start();

		Thread.sleep(3000);

		//Stopping threads for every car
		car1.stopThread();
		car2.stopThread();
		car3.stopThread();
		car4.stopThread();
		car5.stopThread();
		car6.stopThread();
		car7.stopThread();
	}
}
